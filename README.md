# qtaskwarrior

qtaskwarrior is a graphical interface for the [taskwarrior](https://taskwarrior.org/) program using the Qt framework. See the [webpage](https://qtaskwarrior.sourceforge.io/) for more information.
It's developed at [SourceForge](https://sourceforge.net/p/qtaskwarrior/qtaskwarrior/).

# License

The license of each file is specified following the [REUSE sepecification](https://reuse.software/spec/).
Aside from the assets and the metadata, the program is released to the public domain, see LICENSES/Unlicense.txt for more information.
