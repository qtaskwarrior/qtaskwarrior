// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

import QtQuick 2.1
import QtQuick.Layouts 1.1
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras

import io.sourceforge.qtaskwarrior.plasmoid 0.0

Item {
    property int pendingTasksCount: tasksModel.pendingTasksCount
    TasksItemModel {
        id: tasksModel
    }

    Plasmoid.switchWidth: units.gridUnit * 8
    Plasmoid.switchHeight: units.gridUnit * 5

    Plasmoid.fullRepresentation: TaskListPage {
        id: tasksListPage
        onReloadTasksButtonClicked: reloadTasks()
        onCanBeReloadedChanged: {
            if (canBeReloaded) {
                reloadTimer.triggeredOnStart = false // has already run
                reloadTimer.start()
            } else {
                reloadTimer.stop()
            }
        }
    }

    Plasmoid.compactRepresentation: MouseArea {
        onClicked: plasmoid.expanded = !plasmoid.expanded
        ColumnLayout {
            anchors.fill: parent
            PlasmaComponents.Label {
                Layout.alignment: Qt.AlignCenter
                text: pendingTasksCount
            }
        }
    }

    Plasmoid.toolTipSubText: pendingTasksCount === 0 ? "No pending tasks" : pendingTasksCount + " pending tasks"

    Plasmoid.preferredRepresentation: Plasmoid.compactRepresentation
    Component.onCompleted: {
        reloadTimer.start();
    }

    Timer {
        id: reloadTimer
        interval: 1000 * 60 * 60 // run at every hour
        repeat: true
        triggeredOnStart: true
        onTriggered: reloadTasks()
    }

    function reloadTasks()
    {
        console.log("Loading tasks");
        tasksModel.ReloadTasks();
        tasksModel.sort(TasksItemModel.UrgencyIndex, Qt.DescendingOrder);
    }
}
