// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

import QtQuick 2.1
import QtQuick.Layouts 1.1
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras

import io.sourceforge.qtaskwarrior.plasmoid 0.0

PlasmaExtras.ScrollArea {
    signal markTaskAsDone(real taskIndex, string taskUUID)
    signal taskStatusModified()
    anchors {
        bottom: parent.bottom
        left: parent.left
        right: parent.right
        top: parent.bottom
    }

    property Component taskDelegate: Component {
        PlasmaComponents.ListItem {
            property string dueDateStr: tasksModel.formatRelativeDateTime(dueDate)
            property string entryDateStr: tasksModel.formatRelativeDateTime(entryDate)

            Component {
                id: detailsPage
                TaskDetailPage {
                    anchors.fill: parent
                    onMarkTaskAsDoneButtonClicked: markTaskAsDone(index, uuid)
                }
            }

            ColumnLayout {
                PlasmaComponents.Label {
                    text: description
                }

                TaskField {
                    fieldName: qsTr("Urgency:")
                    fieldValue: urgency
                }

                TaskField {
                    fieldName: qsTr("Due on:")
                    fieldValue: dueDateStr
                }

                TaskPriority {
                }

                TaskField {
                    fieldName: qsTr("Project:")
                    fieldValue: project
                }

                TaskTags {
                }
            }

            enabled: true

            onClicked: {
                stack.push(detailsPage);
            }
        }
    }

    onMarkTaskAsDone:
    {
        console.log("Marking task " + taskUUID + ", at index " + taskIndex + ", as done");
        var task = tasksModel.taskAtRow(taskIndex);
        task.status = Task.StatusCompleted;
        tasksModel.SaveTask(task);
        taskStatusModified();
    }

    ListView {
        anchors.fill: parent
        model: tasksModel
        currentIndex: -1
        delegate: taskDelegate
    }
}
