// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

import QtQuick 2.1
import QtQuick.Layouts 1.1
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras

FocusScope {
    signal reloadTasksButtonClicked()
    property bool canBeReloaded: true

    anchors.fill: parent

    PlasmaExtras.Heading {
        id: heading
        text: "Qtaskwarrior"
        level: 4
    }

    PlasmaComponents.ToolButton {
        anchors {
            top: parent.top
            right: parent.right
            bottom: heading.bottom
        }

        id: button
        visible: canBeReloaded
        iconSource: "view-refresh"
        tooltip: qsTr("Reload tasks")
        onClicked: {
            reloadTasksButtonClicked();
        }
    }


    PlasmaComponents.PageStack {
        id: stack
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            top: heading.bottom
        }
        initialPage: TaskListView {
            id: taskListViewPage
            onTaskStatusModified: {
                stack.pop()
                reloadTasksButtonClicked()
            }
        }
        onCurrentPageChanged: {
           canBeReloaded = stack.currentPage === taskListViewPage
        }
    }
}
