// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

import QtQuick 2.1
import QtQuick.Layouts 1.1
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras

Item {
    id: root
    signal markTaskAsDoneButtonClicked()

    anchors.fill: parent
    PlasmaComponents.Button {
        width: parent.width
        id: button
        Layout.fillWidth: true
        iconSource: "go-previous-view"
        text: qsTr("Return to list")
        onClicked: stack.pop()
    }
    ColumnLayout {
        anchors {
            left: root.left
            right: root.right
            top: button.bottom
        }

        PlasmaExtras.Heading {
            text: description
            level: 3
            Layout.fillWidth: true
        }

        TaskField {
            fieldName: qsTr("Urgency:")
            fieldValue: urgency
        }

        TaskField {
            fieldName: qsTr("Entered on:")
            fieldValue: entryDateStr
        }

        TaskField {
            fieldName: qsTr("Due on:")
            fieldValue: dueDateStr
        }

        TaskPriority {
        }

        TaskField {
            fieldName: qsTr("Project:")
            fieldValue: project
        }

        TaskTags {
        }

        TaskAnnotations {
        }

        PlasmaComponents.Button {
            text: qsTr("Done")
            tooltip: qsTr("Mark task as done")
            onClicked: markTaskAsDoneButtonClicked()
            iconName: "task-complete"
        }
    }
}
