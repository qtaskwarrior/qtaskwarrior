// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

import QtQuick 2.1
import QtQuick.Layouts 1.1
import org.kde.plasma.extras 2.0 as PlasmaExtras

ColumnLayout {
    visible: annotations.length > 0

    PlasmaExtras.DescriptiveLabel {
        text: qsTr("Annotations:")
    }

    Repeater {
        model: annotations
        TaskField {
            Layout.leftMargin: units.largeSpacing
            fieldName: modelData.entry.toLocaleDateString(Locale.NarrowFormat)
            fieldValue: modelData.description
        }
    }
}
