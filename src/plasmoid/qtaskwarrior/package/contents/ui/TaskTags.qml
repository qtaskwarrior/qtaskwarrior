// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

import QtQuick 2.1
import QtQuick.Layouts 1.1
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras

RowLayout {
    visible: tags.length > 0

    PlasmaExtras.DescriptiveLabel {
        text: qsTr("Tags:")
    }

    Repeater {
        model: tags
        PlasmaComponents.Label {
            text: "+" + modelData
        }
    }
}
