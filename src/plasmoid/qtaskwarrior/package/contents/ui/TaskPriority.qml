// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

import io.sourceforge.qtaskwarrior.plasmoid 0.0

TaskField {
    visible: priority !== Task.NoPriority
    fieldName: qsTr("Priority:")
    fieldValue: priority === Task.HightPriority ?
                    qsTr("High") :
                (
                priority === Task.MediumPriority ?
                    qsTr("Medium") :
                (
                priority === Task.LowPriority ?
                    qsTr("Low") :
                    qsTr("No priority")
                ))

}
