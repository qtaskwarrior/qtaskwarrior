// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

import QtQuick 2.1
import QtQuick.Layouts 1.1
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras

RowLayout {
    property string fieldName
    property string fieldValue

    Layout.fillWidth: true
    visible: fieldValue !== ""

    PlasmaExtras.DescriptiveLabel {
        text: fieldName
    }
    PlasmaComponents.Label {
        text: fieldValue
    }
}
