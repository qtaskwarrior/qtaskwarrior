// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

#ifndef QTASKWARRIORPLUGIN_H
#define QTASKWARRIORPLUGIN_H

#include <QQmlExtensionPlugin>

class qtaskwarriorPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri) override;
};

#endif // QTASKWARRIORPLUGIN_H
