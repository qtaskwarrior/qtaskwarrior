// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

#include "qtaskwarriorplugin.h"

#include <QQmlEngine>
#include <QtQml>

#include "../../../lib/tasksitemmodel.h"

void qtaskwarriorPlugin::registerTypes(const char* uri)
{
    Q_ASSERT(uri == QLatin1String("io.sourceforge.qtaskwarrior.plasmoid"));

    qmlRegisterType<TasksItemModel>(uri, 0, 0, "TasksItemModel");
    qmlRegisterType<Task>(uri, 0, 0, "Task");
}
