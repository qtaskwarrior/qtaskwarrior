<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../app/mainwindow.ui" line="14"/>
        <source>Tasks</source>
        <translation>Tareas</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="53"/>
        <source>Fi&amp;lter</source>
        <translation>&amp;Filtrar</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="63"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="70"/>
        <source>&amp;Task</source>
        <translation>&amp;Tarea</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="112"/>
        <source>&amp;Reload</source>
        <translation>&amp;Recargar</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="123"/>
        <source>&amp;Pending</source>
        <translation>&amp;Pendientes</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="131"/>
        <source>&amp;Completed</source>
        <translation>&amp;Completadas</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="134"/>
        <source>Completed</source>
        <translation>Completadas</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="142"/>
        <source>D&amp;eleted</source>
        <translation>&amp;Eliminadas</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="150"/>
        <source>&amp;Done</source>
        <translation>&amp;Hecho</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="162"/>
        <source>D&amp;elete</source>
        <translation>&amp;Eliminar</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="173"/>
        <source>&amp;Waiting</source>
        <translation>E&amp;sperando</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="194"/>
        <source>Start</source>
        <translation>Empezar</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="197"/>
        <source>Start task</source>
        <translation>Empezar tarea</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="206"/>
        <source>Stop</source>
        <translation>Parar</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="209"/>
        <source>Stop task</source>
        <translation>Parar tarea</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="153"/>
        <source>Mark task as done</source>
        <translation>Marca la tarea como hecha</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.cpp" line="149"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="165"/>
        <source>Delete task</source>
        <translation>Eliminar tarea</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="182"/>
        <source>&amp;New</source>
        <translation>&amp;Nueva</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.ui" line="185"/>
        <source>Create new task</source>
        <translation>Crea una nueva tarea</translation>
    </message>
    <message>
        <location filename="../app/mainwindow.cpp" line="147"/>
        <source>Do you really want to delete the task &quot;%1&quot;?</source>
        <translation>¿Quiere borrar realmente la tarea &quot;%1&quot;?</translation>
    </message>
</context>
<context>
    <name>TaskAnnotations</name>
    <message>
        <location filename="../plasmoid/qtaskwarrior/package/contents/ui/TaskAnnotations.qml" line="13"/>
        <source>Annotations:</source>
        <translation>Anotaciones:</translation>
    </message>
</context>
<context>
    <name>TaskDetailPage</name>
    <message>
        <location filename="../plasmoid/qtaskwarrior/package/contents/ui/TaskDetailPage.qml" line="21"/>
        <source>Return to list</source>
        <translation>Volver a la lista</translation>
    </message>
    <message>
        <location filename="../plasmoid/qtaskwarrior/package/contents/ui/TaskDetailPage.qml" line="38"/>
        <source>Urgency:</source>
        <translation>Urgencia:</translation>
    </message>
    <message>
        <location filename="../plasmoid/qtaskwarrior/package/contents/ui/TaskDetailPage.qml" line="43"/>
        <source>Entered on:</source>
        <translation>Introducido en:</translation>
    </message>
    <message>
        <location filename="../plasmoid/qtaskwarrior/package/contents/ui/TaskDetailPage.qml" line="48"/>
        <source>Due on:</source>
        <translation>Fecha límite:</translation>
    </message>
    <message>
        <location filename="../plasmoid/qtaskwarrior/package/contents/ui/TaskDetailPage.qml" line="56"/>
        <source>Project:</source>
        <translation>Proyecto:</translation>
    </message>
    <message>
        <location filename="../plasmoid/qtaskwarrior/package/contents/ui/TaskDetailPage.qml" line="67"/>
        <source>Done</source>
        <translation>Hecho</translation>
    </message>
    <message>
        <location filename="../plasmoid/qtaskwarrior/package/contents/ui/TaskDetailPage.qml" line="68"/>
        <source>Mark task as done</source>
        <translation>Marca la tarea como hecha</translation>
    </message>
</context>
<context>
    <name>TaskEditForm</name>
    <message>
        <location filename="../app/taskeditform.ui" line="14"/>
        <source>Edit task</source>
        <translation>Editar tarea</translation>
    </message>
    <message>
        <location filename="../app/taskeditform.ui" line="20"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../app/taskeditform.ui" line="60"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../app/taskeditform.ui" line="80"/>
        <source>Due date</source>
        <translation>Fecha límite</translation>
    </message>
    <message>
        <location filename="../app/taskeditform.ui" line="43"/>
        <source>&amp;UUID</source>
        <translation>&amp;UUID</translation>
    </message>
    <message>
        <location filename="../app/taskeditform.ui" line="120"/>
        <source>Priorit&amp;y</source>
        <translation>Priorida&amp;d</translation>
    </message>
    <message>
        <location filename="../app/taskeditform.ui" line="134"/>
        <source>No priority</source>
        <translation>Sin prioridad</translation>
    </message>
    <message>
        <location filename="../app/taskeditform.ui" line="139"/>
        <source>Low</source>
        <translation>Baja</translation>
    </message>
    <message>
        <location filename="../app/taskeditform.ui" line="144"/>
        <source>Medium</source>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../app/taskeditform.ui" line="149"/>
        <source>High</source>
        <translation>Alta</translation>
    </message>
    <message>
        <location filename="../app/taskeditform.ui" line="100"/>
        <source>Wait until</source>
        <translation>Esperar hasta</translation>
    </message>
    <message>
        <location filename="../app/taskeditform.ui" line="170"/>
        <source>Pro&amp;ject</source>
        <translation>&amp;Proyecto</translation>
    </message>
    <message>
        <location filename="../app/taskeditform.ui" line="180"/>
        <source>Tags</source>
        <translation>Etiquetas</translation>
    </message>
    <message>
        <location filename="../app/taskeditform.ui" line="73"/>
        <source>Entered on</source>
        <translation>Introducido en</translation>
    </message>
</context>
<context>
    <name>TaskListPage</name>
    <message>
        <location filename="../plasmoid/qtaskwarrior/package/contents/ui/TaskListPage.qml" line="33"/>
        <source>Reload tasks</source>
        <translation>Recargar tareas</translation>
    </message>
</context>
<context>
    <name>TaskListView</name>
    <message>
        <location filename="../plasmoid/qtaskwarrior/package/contents/ui/TaskListView.qml" line="42"/>
        <source>Urgency:</source>
        <translation>Urgencia:</translation>
    </message>
    <message>
        <location filename="../plasmoid/qtaskwarrior/package/contents/ui/TaskListView.qml" line="47"/>
        <source>Due on:</source>
        <translation>Fecha límite:</translation>
    </message>
    <message>
        <location filename="../plasmoid/qtaskwarrior/package/contents/ui/TaskListView.qml" line="55"/>
        <source>Project:</source>
        <translation>Proyecto:</translation>
    </message>
</context>
<context>
    <name>TaskPriority</name>
    <message>
        <location filename="../plasmoid/qtaskwarrior/package/contents/ui/TaskPriority.qml" line="9"/>
        <source>Priority:</source>
        <translation>Prioridad:</translation>
    </message>
    <message>
        <location filename="../plasmoid/qtaskwarrior/package/contents/ui/TaskPriority.qml" line="11"/>
        <source>High</source>
        <translation>Alta</translation>
    </message>
    <message>
        <location filename="../plasmoid/qtaskwarrior/package/contents/ui/TaskPriority.qml" line="14"/>
        <source>Medium</source>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../plasmoid/qtaskwarrior/package/contents/ui/TaskPriority.qml" line="17"/>
        <source>Low</source>
        <translation>Baja</translation>
    </message>
    <message>
        <location filename="../plasmoid/qtaskwarrior/package/contents/ui/TaskPriority.qml" line="18"/>
        <source>No priority</source>
        <translation>Sin prioridad</translation>
    </message>
</context>
<context>
    <name>TaskTags</name>
    <message>
        <location filename="../plasmoid/qtaskwarrior/package/contents/ui/TaskTags.qml" line="14"/>
        <source>Tags:</source>
        <translation>Etiquetas:</translation>
    </message>
</context>
<context>
    <name>TasksItemModel</name>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="240"/>
        <source>Completed</source>
        <translation>Completada</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="242"/>
        <source>Pending</source>
        <translation>Pendiente</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="244"/>
        <source>Deleted</source>
        <translation>Eliminada</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="246"/>
        <source>Waiting</source>
        <translation>Esperando</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="248"/>
        <source>Unknown status</source>
        <translation>Estado desconocido</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="263"/>
        <source>No priority</source>
        <translation>Sin prioridad</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="265"/>
        <source>Low</source>
        <translation>Baja</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="267"/>
        <source>Medium</source>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="269"/>
        <source>High</source>
        <translation>Alta</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="332"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="334"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="336"/>
        <source>Due date</source>
        <translation>Fecha límite</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="338"/>
        <source>Urgency</source>
        <translation>Urgencia</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="340"/>
        <source>UUID</source>
        <translation>UUID</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="342"/>
        <source>Status</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="344"/>
        <source>Priority</source>
        <translation>Prioridad</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="346"/>
        <source>Wait until</source>
        <translation>Esperar hasta</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="348"/>
        <source>Project</source>
        <translation>Proyecto</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="350"/>
        <source>Tags</source>
        <translation>Etiquetas</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="352"/>
        <source>Annotations</source>
        <translation>Anotaciones</translation>
    </message>
    <message>
        <location filename="../lib/tasksitemmodel.cpp" line="354"/>
        <source>Entry date</source>
        <translation>Fecha de introducción</translation>
    </message>
</context>
</TS>
