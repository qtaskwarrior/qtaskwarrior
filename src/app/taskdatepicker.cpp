// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

#include "taskdatepicker.h"
#include "ui_taskdatepicker.h"

TaskDatePicker::TaskDatePicker(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TaskDatePicker)
{
    ui->setupUi(this);
    connect(ui->checkBox, &QCheckBox::stateChanged, this, &TaskDatePicker::checkBoxStateChanged);
    connect(ui->dateTimeEdit, &KDateTimeEdit::dateTimeChanged, this, &TaskDatePicker::dateTimeChanged);
}

TaskDatePicker::~TaskDatePicker()
{
    delete ui;
}

QDateTime TaskDatePicker::Date()
{
    if (ui->checkBox->checkState() == Qt::Checked)
    {
        return ui->dateTimeEdit->dateTime();
    }
    else
    {
        return QDateTime();
    }
}
void TaskDatePicker::setDate(QDateTime dat)
{
    ui->checkBox->setChecked(!dat.isNull());
    ui->dateTimeEdit->setDateTime(dat);
}

void TaskDatePicker::checkBoxStateChanged(bool checked)
{
    ui->dateTimeEdit->setEnabled(checked);
    if (!checked)
    {
        emit DateChanged();
    }
}

void TaskDatePicker::dateTimeChanged(QDateTime)
{
    emit DateChanged();
}
