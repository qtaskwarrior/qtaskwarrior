# SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
#
# SPDX-License-Identifier: Unlicense

set(qtaskwarrior_SRCS
    main.cpp
    mainwindow.cpp
    taskeditform.cpp
    taskdatepicker.cpp

    ../config.h
)

qt5_add_resources(qtaskwarrior_SRCS qtaskwarrior.qrc)

set(qtaskwarrior_FORMS
    mainwindow.ui
    taskeditform.ui
    taskdatepicker.ui
    )

ecm_add_app_icon(qtaskwarrior_SRCS ICONS ${qtaskwarrior_ICONS})


add_executable(qtaskwarrior-bin
    ${qtaskwarrior_SRCS}
    ${qtaskwarrior_FORMS}
    )

set_target_properties(qtaskwarrior-bin
    PROPERTIES OUTPUT_NAME qtaskwarrior)


target_link_libraries(qtaskwarrior-bin
    qtaskwarrior-lib
    Qt5::Core Qt5::Gui Qt5::Widgets
    KF5::WidgetsAddons
    KF5::XmlGui
    )

target_compile_features(qtaskwarrior-bin
    PRIVATE
        cxx_nullptr
    )

install(TARGETS qtaskwarrior-bin
    ${KDE_INSTALL_TARGETS_DEFAULT_ARGS})
