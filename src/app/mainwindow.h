// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QItemSelection>

#include "../lib/tasksitemmodel.h"
#include "../lib/task.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    TasksItemModel *tasksModel;
    void LoadTasks();
    void updateStatusFilter(bool checked, Task::TaskStatus status);
    void ReloadTasks();
    void editTask(QModelIndex index);
public slots:
    void doubleClickTable(const QModelIndex &index);
    void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
private slots:
    void on_actionReload_triggered();
    void on_actionPending_triggered(bool checked);
    void on_actionCompleted_triggered(bool checked);
    void on_actionDeleted_triggered(bool checked);
    void on_actionWaiting_triggered(bool checked);
    void on_actionMarkDone_triggered();
    void on_actionDeleteTask_triggered();
    void on_actionNew_triggered();
    void on_actionStart_triggered();
    void on_actionStop_triggered();
};

#endif // MAINWINDOW_H
