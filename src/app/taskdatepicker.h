// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

#ifndef TASKDATEPICKER_H
#define TASKDATEPICKER_H

#include <QWidget>
#include <QDateTime>

namespace Ui {
class TaskDatePicker;
}

class TaskDatePicker : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QDateTime Date READ Date WRITE setDate NOTIFY DateChanged USER true)
public:
    explicit TaskDatePicker(QWidget *parent = nullptr);
    ~TaskDatePicker();

    QDateTime Date();
    void setDate(QDateTime dat);

signals:
    void DateChanged();

private:
    Ui::TaskDatePicker *ui;

    QDateTime m_date;
private slots:
    void checkBoxStateChanged(bool checked);
    void dateTimeChanged(QDateTime);
};

#endif // TASKDATEPICKER_H
