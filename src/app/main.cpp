// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>
#include <QDir>
#include <QCommandLineParser>

#include <KAboutData>

#include "../config.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QGuiApplication::setWindowIcon(QIcon(QStringLiteral(":/qtaskwarrior.png")));

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
            QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QTranslator myappTranslator;
    myappTranslator.load("qtaskwarrior_" + QLocale::system().name(), TRANSLATIONS_PATH);
    app.installTranslator(&myappTranslator);

    KAboutData aboutData("qtaskwarrior", "qtaskwarrior",
                         QTASKWARRIOR_VERSION,
                         "Qt interface for taskwarrior",
                         KAboutLicense::Custom,
                         QString(), QString(),
                         "https://qtaskwarrior.sourceforge.io/",
                         "https://sourceforge.net/p/qtaskwarrior/tickets/");

    aboutData.addAuthor(QStringLiteral("Jaime Marquínez Ferrándiz"), "Main developer");

    aboutData.setLicenseTextFile(QStringLiteral(":/LICENSE"));

    QCommandLineParser parser;
    KAboutData::setApplicationData(aboutData);
    aboutData.setupCommandLine(&parser);

    parser.process(app);
    aboutData.processCommandLine(&parser);

    MainWindow w;
    w.show();

    return app.exec();
}
