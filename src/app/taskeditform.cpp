// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

#include "taskeditform.h"
#include "ui_taskeditform.h"

#include "../lib/tasksitemmodel.h"

TaskEditForm::TaskEditForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TaskEditForm)
{
    ui->setupUi(this);
    mapper = new QDataWidgetMapper(this);
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &TaskEditForm::dialogButtonBox_accepted);
    connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &TaskEditForm::dialogButtonBox_rejected);
}

TaskEditForm::~TaskEditForm()
{
    delete ui;
}

QAbstractItemModel * TaskEditForm::Model()
{
    return mapper->model();
}

void TaskEditForm::setModel(QAbstractItemModel *model)
{
    mapper->setModel(model);
    mapper->addMapping(ui->idSpinBox, TasksItemModel::IdIndex);
    mapper->addMapping(ui->descriptionLineEdit, TasksItemModel::DescriptionIndex);
    mapper->addMapping(ui->entryDateEdit, TasksItemModel::EntryDateIndex);
    mapper->addMapping(ui->dueDate, TasksItemModel::DueDateIndex);
    mapper->addMapping(ui->lineUUID, TasksItemModel::UUIDIndex);
    mapper->addMapping(ui->priorityComboBox, TasksItemModel::PriorityIndex, "currentIndex");
    mapper->addMapping(ui->waitDate, TasksItemModel::WaitDateIndex);

    ui->comboProject->clear();
    ui->comboProject->addItems(qobject_cast<TasksItemModel*>(model)->Projects());
    mapper->addMapping(ui->comboProject, TasksItemModel::ProjectIndex);


    QComboBox *comboTags = new QComboBox(ui->tagsListWidget);
    comboTags->setEditable(true);
    comboTags->addItems(qobject_cast<TasksItemModel*>(model)->Tags());
    ui->tagsListWidget->setCustomEditor(comboTags);
    mapper->addMapping(ui->tagsListWidget, TasksItemModel::TagsIndex);

    mapper->setSubmitPolicy(QDataWidgetMapper::ManualSubmit);
}

void TaskEditForm::setIndex(const QModelIndex &index)
{
    mapper->setCurrentIndex(index.row());
}

void TaskEditForm::dialogButtonBox_accepted()
{
    mapper->submit();
    emit accepted();
}

void TaskEditForm::dialogButtonBox_rejected()
{
    if (ui->lineUUID->text().isEmpty())
    {
        mapper->model()->removeRow(mapper->currentIndex());
    }
    emit rejected();
}
