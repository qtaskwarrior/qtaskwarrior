// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QStandardItemModel>
#include <QDialog>
#include <QMessageBox>
#include <QDataWidgetMapper>
#include <QPushButton>

#include <KHelpMenu>

#include "taskeditform.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);    
    auto helpMenu = new KHelpMenu(this);
    ui->menuBar->addMenu(helpMenu->menu());

    tasksModel = new TasksItemModel(this);
    LoadTasks();
    connect(ui->tasksTableView->selectionModel(), &QItemSelectionModel::selectionChanged, this, &MainWindow::selectionChanged);

    ui->actionNew->setShortcut(QKeySequence::New);
    ui->actionDeleteTask->setShortcut(QKeySequence::Delete);
    ui->actionReload->setShortcut(QKeySequence::Refresh);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::LoadTasks() {
    tasksModel->LoadTasks();
    ui->tasksTableView->setModel(tasksModel);
    ui->tasksTableView->setColumnHidden(TasksItemModel::UUIDIndex, true);
    ui->tasksTableView->setColumnHidden(TasksItemModel::IdIndex, true);
    ui->tasksTableView->setColumnHidden(TasksItemModel::AnnotationsIndex, true);
    ui->tasksTableView->sortByColumn(TasksItemModel::UrgencyIndex, Qt::DescendingOrder);
    return;
}

void MainWindow::ReloadTasks()
{
    this->tasksModel->ReloadTasks();
    // Sort again with the selected column
    this->ui->tasksTableView->setSortingEnabled(true);
}

void MainWindow::doubleClickTable(const QModelIndex &index)
{
    return this->editTask(index);
}

void MainWindow::selectionChanged(const QItemSelection &selected, const QItemSelection &)
{
    ui->actionMarkDone->setEnabled(false);
    ui->actionStart->setEnabled(false);
    ui->actionStop->setEnabled(false);
    ui->actionDeleteTask->setEnabled(true);
    foreach(QModelIndex index,selected.indexes())
    {
        Task *task = tasksModel->taskAtIndex(index);
        if (task->Status() == Task::StatusPending || task->Status() == Task::StatusWaiting)
        {
            ui->actionMarkDone->setEnabled(true);
        }
        if (task->Status() == Task::StatusPending)
        {
            if (task->Running())
            {
                ui->actionStop->setEnabled(true);
            }
            else {
                ui->actionStart->setEnabled(true);
            }
        }
        if (task->Status() == Task::StatusDeleted)
        {
            ui->actionDeleteTask->setEnabled(false);
        }
    }
}

void MainWindow::on_actionReload_triggered()
{
    this->ReloadTasks();
}


void MainWindow::updateStatusFilter(bool checked, Task::TaskStatus status)
{
    Task::TaskStatuses filter = tasksModel->StatusFilter();
    if (checked) filter |= status;
    else filter &= ~status;
    tasksModel->setStatusFilter(filter);
}

void MainWindow::on_actionPending_triggered(bool checked)
{
    updateStatusFilter(checked, Task::StatusPending);
}

void MainWindow::on_actionCompleted_triggered(bool checked)
{
    updateStatusFilter(checked, Task::StatusCompleted);
}

void MainWindow::on_actionDeleted_triggered(bool checked)
{
    updateStatusFilter(checked, Task::StatusDeleted);
}

void MainWindow::on_actionWaiting_triggered(bool checked)
{
    updateStatusFilter(checked, Task::StatusWaiting);
}

void MainWindow::on_actionMarkDone_triggered()
{
    QModelIndexList indexList = ui->tasksTableView->selectionModel()->selectedRows();
    foreach(QModelIndex index,indexList)
    {
        Task *task = tasksModel->taskAtIndex(index);
        task->setStatus(Task::StatusCompleted);
        tasksModel->SaveTask(task);
    }
    this->ReloadTasks();
}

void MainWindow::on_actionDeleteTask_triggered()
{
    bool didDeleteSomeTask = false;
    QModelIndexList indexList = ui->tasksTableView->selectionModel()->selectedRows();

    foreach(QModelIndex index,indexList)
    {
        Task *task = tasksModel->taskAtIndex(index);
        QMessageBox msgBox;
        msgBox.setText(tr("Do you really want to delete the task \"%1\"?")
                       .arg(task->Description()));
        QPushButton *deleteButton = new QPushButton(QIcon::fromTheme("edit-delete"), tr("Delete"), &msgBox);
        msgBox.addButton(deleteButton, QMessageBox::DestructiveRole);
        msgBox.addButton(QMessageBox::Cancel);
        msgBox.exec();
        if (msgBox.clickedButton() == deleteButton)
        {
            task->setStatus(Task::StatusDeleted);
            tasksModel->SaveTask(task);
            didDeleteSomeTask = true;
        }
    }
    if (didDeleteSomeTask)
    {
        this->ReloadTasks();
    }
}

void MainWindow::on_actionNew_triggered()
{
    QModelIndex index = this->tasksModel->newTask();
    return this->editTask(index);
}

void MainWindow::editTask(QModelIndex index)
{
    QDialog dialog(this);
    TaskEditForm *form = new TaskEditForm(&dialog);
    form->setModel(tasksModel);
    form->setIndex(index);
    connect(form, &TaskEditForm::accepted, &dialog, &QDialog::accept);
    connect(form, &TaskEditForm::rejected, &dialog, &QDialog::reject);

    dialog.setModal(true);
    dialog.exec();
    return;
}

void MainWindow::on_actionStart_triggered()
{
    QModelIndexList indexList = ui->tasksTableView->selectionModel()->selectedRows();
    foreach(QModelIndex index,indexList)
    {
        Task *task = tasksModel->taskAtIndex(index);
        tasksModel->StartTask(task);
    }
    this->ReloadTasks();
}

void MainWindow::on_actionStop_triggered()
{
    QModelIndexList indexList = ui->tasksTableView->selectionModel()->selectedRows();
    foreach(QModelIndex index,indexList)
    {
        Task *task = tasksModel->taskAtIndex(index);
        tasksModel->StopTask(task);
    }
    this->ReloadTasks();
}
