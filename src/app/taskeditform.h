// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

#ifndef TASKEDITFORM_H
#define TASKEDITFORM_H

#include <QWidget>
#include <QAbstractItemModel>
#include <QDataWidgetMapper>

namespace Ui {
class TaskEditForm;
}

class TaskEditForm : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QAbstractItemModel* Model READ Model WRITE setModel)

public:
    explicit TaskEditForm(QWidget *parent = nullptr);
    ~TaskEditForm();

    QAbstractItemModel* Model();
    void setModel(QAbstractItemModel *model);
    void setIndex(const QModelIndex &index);

private:
    Ui::TaskEditForm *ui;
    QDataWidgetMapper *mapper;

private slots:
    void dialogButtonBox_accepted();
    void dialogButtonBox_rejected();
signals:
    void accepted();
    void rejected();

};

#endif // TASKEDITFORM_H
