// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

#include "taskannotation.h"

#include <QJsonObject>

TaskAnnotation::TaskAnnotation(QDateTime entry, QString description)
{
    m_entry = entry;
    m_description = description;
}

TaskAnnotation::TaskAnnotation()
{
}


TaskAnnotation::TaskAnnotation(const TaskAnnotation &other)
{
    m_entry = other.m_entry;
    m_description = other.m_description;
}

TaskAnnotation::~TaskAnnotation()
{

}

void TaskAnnotation::operator=(const TaskAnnotation &annotation)
{
    this->m_entry = annotation.m_entry;
    this->m_description = annotation.m_description;
}
