// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

#ifndef TASKSITEMMODEL_H
#define TASKSITEMMODEL_H

#include <QAbstractTableModel>
#include <QJsonArray>
#include <QLocale>
#include <QProcess>
#include <QSet>

#include "task.h"



class TasksItemModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_PROPERTY(Task::TaskStatuses StatusFilter READ StatusFilter WRITE setStatusFilter)
    Q_PROPERTY(QStringList Projects READ Projects)
    Q_PROPERTY(int pendingTasksCount MEMBER m_pendingTasksCount NOTIFY pendingTasksCountChanged)
public:
    enum TaskModelIndex {
        IdIndex = 0,
        UrgencyIndex = 1,
        DescriptionIndex = 2,
        DueDateIndex = 3,
        UUIDIndex = 4,
        StatusIndex = 5,
        PriorityIndex = 6,
        WaitDateIndex = 7,
        ProjectIndex = 8,
        TagsIndex = 9,
        AnnotationsIndex = 10,
        EntryDateIndex = 11,
        _LAST_INDEX = 11
    };
    Q_ENUM(TaskModelIndex)

    TasksItemModel(QObject *parent = nullptr);

    QProcess* buildTaskProcess();
    Q_INVOKABLE void LoadTasks();
    Q_INVOKABLE void ReloadTasks();
    Q_INVOKABLE void SaveTask(Task *task);
    void StartTask(Task *task);
    void StopTask(Task *task);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QHash<int, QByteArray> roleNames() const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex & index) const ;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    Q_INVOKABLE void sort(int column, Qt::SortOrder order = Qt::AscendingOrder);
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());

    Task* taskAtIndex(const QModelIndex &index) const;
    Q_INVOKABLE Task* taskAtRow(const int &row);

    Task::TaskStatuses StatusFilter();
    void setStatusFilter(Task::TaskStatuses filter);

    QStringList Projects();
    QStringList Tags();

    QModelIndex newTask();

    Q_INVOKABLE QString formatRelativeDateTime(QDateTime date, QLocale::FormatType format = QLocale::ShortFormat) const;
    bool submit();
signals:
    void pendingTasksCountChanged();
private:
    QProcess* runCommandOnTask(Task *task, QStringList arguments);
    QList<Task*> readTasksFromOutput(QProcess *process);
    bool AddTask(Task *task, Task *taskToReplace = nullptr);

    QList<Task*> tasks;
    QSet<QString> m_projects;
    QSet<QString> m_tags;
    int m_pendingTasksCount = 0;
    Task::TaskStatuses _statusFilter = Task::StatusPending;
};

#endif // TASKSITEMMODEL_H
