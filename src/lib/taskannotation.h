// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

#ifndef TASKANNOTATION_H
#define TASKANNOTATION_H

#include <QObject>
#include <QDateTime>

class TaskAnnotation
{
    Q_GADGET
    Q_PROPERTY(QDateTime entry MEMBER m_entry)
    Q_PROPERTY(QString description MEMBER m_description)
public:
    TaskAnnotation(QDateTime entry, QString description);
    TaskAnnotation();
    TaskAnnotation(const TaskAnnotation &other);
    ~TaskAnnotation();

    void operator=(const TaskAnnotation &annotation);
private:
    QDateTime m_entry;
    QString m_description;
};

Q_DECLARE_METATYPE(TaskAnnotation);

#endif // TASKANNOTATION_H
