// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

#include "task.h"

#include <QJsonArray>

const QString TASKWARRIOR_DATETIME_FORMAT = "yyyyMMdd'T'hhmmss'Z'";

Task::Task(QJsonObject jsonObj, QObject *parent) : QObject(parent)
{
    this->m_jsonObj = jsonObj;
    this->_hasUnsavedChanges = false;
}

Task::Task(QObject *parent) : Task(QJsonObject(), parent)
{
}

QJsonObject Task::toJSON()
{
    return this->m_jsonObj;
}

int Task::Id()
{
    return m_jsonObj.value("id").toInt();
}

void Task::setId(int id)
{
    m_jsonObj.insert("id", id);
}

QString Task::UUID()
{
    return m_jsonObj.value("uuid").toString();
}

void Task::setUUID(QString uuid)
{
    m_jsonObj.insert("uuid", uuid);
}

QString Task::Description()
{
    return m_jsonObj.value("description").toString();
}

void Task::setDescription(QString description)
{
    m_jsonObj.insert("description", description);
}

double Task::Urgency()
{
    return m_jsonObj.value("urgency").toDouble();
}

void Task::setUrgency(double urgency)
{
    m_jsonObj.insert("urgency", urgency);
}

QDateTime Task::EntryDate()
{
    return this->getDateField("entry");
}

QDateTime Task::DueDate()
{
    return this->getDateField("due");
}

void Task::setDueDate(QDateTime dueDate)
{
    this->insertDate("due", dueDate);
}

QDateTime Task::WaitDate()
{
    return this->getDateField("wait");
}

void Task::setWaitDate(QDateTime waitDate)
{
    this->insertDate("wait", waitDate);
}

Task::TaskStatus Task::Status()
{
    QString _status = m_jsonObj.value("status").toString();
    if (_status == "completed")
        return StatusCompleted;
    // TODO: the waiting state was removed in version 2.6.0, this check should be simplified in the future
    else if(_status == "waiting" || (!this->WaitDate().isNull() && this->WaitDate() > QDateTime::currentDateTime()))
        return StatusWaiting;
    else if(_status == "pending")
        return StatusPending;
    else if(_status == "deleted")
        return StatusDeleted;
    else
        return StatusUnknown;
}

void Task::setStatus(Task::TaskStatus status)
{
    QString _status_s;
    switch (status) {
    case StatusCompleted:
        _status_s = "completed";
        break;
    case StatusPending:
        _status_s = "pending";
        break;
    case StatusDeleted:
        _status_s = "deleted";
        break;
    case StatusWaiting:
        _status_s = "waiting";
        break;
    default:
        break;
    }
    m_jsonObj.insert("status", _status_s);
}

Task::TaskPriority Task::Priority()
{
    QString _priority = m_jsonObj.value("priority").toString();
    if (_priority == "L")
        return LowPriority;
    else if (_priority == "M")
        return MediumPriority;
    else if (_priority == "H")
        return HightPriority;
    else
        return NoPriority;
}

void Task::setPriority(Task::TaskPriority priority)
{
    QString _priority;
    switch (priority) {
        break;
    case LowPriority:
        _priority = "L";
        break;
    case MediumPriority:
        _priority = "M";
        break;
    case HightPriority:
        _priority = "H";
        break;
    case NoPriority:
    default:
        _priority = "";
        break;
    }
    m_jsonObj.insert("priority", _priority);
}

QString Task::Project()
{
    return m_jsonObj.value("project").toString();
}

void Task::setProject(QString project)
{
     m_jsonObj.insert("project", project);
}

QStringList Task::Tags()
{
    QStringList result;
    QJsonArray array = m_jsonObj.value("tags").toArray();
    foreach(QJsonValue obj, array)
    {
        result << obj.toString();
    }
    return result;
}

void Task::setTags(QStringList tags)
{
    m_jsonObj.insert("tags", QJsonArray::fromStringList(tags));
}

bool Task::Running()
{
    auto val = m_jsonObj.value("start");
    return !val.isUndefined() && !val.isNull();
}

QList<TaskAnnotation> Task::Annotations()
{
    QList<TaskAnnotation> annotations;
    QJsonArray array = m_jsonObj.value("annotations").toArray();
    foreach(QJsonValue val, array)
    {
        QJsonObject obj = val.toObject();
        QDateTime entryDate = this->getDateField("entry", obj);
        QString description = obj.value("description").toString();
        TaskAnnotation annotation(entryDate, description);
        annotations << annotation;
    }
    return annotations;
}

QDateTime Task::getDateField(const QString &key)
{
    return this->getDateField(key, m_jsonObj);
}

QDateTime Task::getDateField(const QString &key, QJsonObject &jsonObject)
{
    QString dueDate = jsonObject.value(key).toString();
    QDateTime date;
    if (!dueDate.isNull()) {
        date = QDateTime::fromString(dueDate, TASKWARRIOR_DATETIME_FORMAT);
    }
    return date;
}

QJsonObject::iterator Task::insertDate(const QString &key, const QDateTime &date)
{
    if (date.isNull())
    {
        return m_jsonObj.insert(key,  QJsonValue::Undefined);
    }
    else
    {
        return m_jsonObj.insert(key,  date.toString(TASKWARRIOR_DATETIME_FORMAT));
    }
}
