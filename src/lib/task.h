// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

#ifndef TASK_H
#define TASK_H

#include <QObject>
#include <QJsonObject>
#include <QDateTime>
#include "taskannotation.h"

class Task : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int Id READ Id WRITE setId)
    Q_PROPERTY(QString UUID READ UUID WRITE setUUID)
    Q_PROPERTY(QString Description READ Description WRITE setDescription)
    Q_PROPERTY(double Urgency READ Urgency WRITE setUrgency)
    Q_PROPERTY(QDateTime EntryDate READ EntryDate)
    Q_PROPERTY(QDateTime DueDate READ DueDate WRITE setDueDate)
    Q_PROPERTY(QDateTime WaitDate READ WaitDate WRITE setWaitDate)
    Q_PROPERTY(Task::TaskStatus status READ Status WRITE setStatus)
    Q_PROPERTY(Task::TaskPriority Priority READ Priority WRITE setPriority)
    Q_PROPERTY(QString Project READ Project WRITE setProject)
    Q_PROPERTY(QStringList Tags READ Tags WRITE setTags)
    Q_PROPERTY(bool Running READ Running)
    Q_PROPERTY(QList<TaskAnnotation> Annotations READ Annotations)
public:
    enum TaskStatus {
        StatusPending   = 1 << 1,
        StatusCompleted = 1 << 2,
        StatusDeleted   = 1 << 3,
        StatusWaiting   = 1 << 4,
        StatusUnknown   = 1 << 5
    };
    Q_ENUM(TaskStatus)
    Q_DECLARE_FLAGS(TaskStatuses, TaskStatus)
    enum TaskPriority {
        NoPriority = 0 ,
        LowPriority = 1,
        MediumPriority = 2,
        HightPriority = 3
    };
    Q_ENUM(TaskPriority)
    explicit Task(QJsonObject jsonObj, QObject *parent = nullptr);
    Task(QObject *parent = nullptr);
    QJsonObject toJSON();
    int Id();
    void setId(int id);
    QString UUID();
    void setUUID(QString uuid);
    QString Description();
    void setDescription(QString description);
    double Urgency();
    void setUrgency(double urgency);
    QDateTime EntryDate();
    QDateTime DueDate();
    void setDueDate(QDateTime dueDate);
    QDateTime WaitDate();
    void setWaitDate(QDateTime waitDate);
    Task::TaskStatus Status();
    void setStatus(Task::TaskStatus status);
    Task::TaskPriority Priority();
    void setPriority(Task::TaskPriority priority);
    QString Project();
    void setProject(QString project);
    QStringList Tags();
    void setTags(QStringList tags);
    bool Running();
    QList<TaskAnnotation> Annotations();
    bool _hasUnsavedChanges;
signals:

public slots:
private:
    QJsonObject m_jsonObj;
    QDateTime getDateField(const QString &key);
    QDateTime getDateField(const QString &key, QJsonObject &jsonObject);
    QJsonObject::iterator insertDate(const QString &key, const QDateTime &date);
};

Q_DECLARE_OPERATORS_FOR_FLAGS(Task::TaskStatuses)

#endif // TASK_H
