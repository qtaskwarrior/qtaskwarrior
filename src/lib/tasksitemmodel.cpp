// SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
//
// SPDX-License-Identifier: Unlicense

#include "tasksitemmodel.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QDateTime>
#include <QDir>

#include <KFormat>

TasksItemModel::TasksItemModel(QObject *parent)
    :QAbstractTableModel(parent)
{

}

QProcess* TasksItemModel::buildTaskProcess()
{
    QProcess *process = new QProcess(this);
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
#ifdef QT_DEBUG
    QString taskdata_dir = QDir::home().filePath("code/qtaskwarrior/taskdata");
    env.insert("TASKDATA", taskdata_dir);
#endif
    process->setProcessEnvironment(env);
    process->setProgram("task");
    return process;
}

void TasksItemModel::LoadTasks()
{
    int pendingTasksCount = 0;
    QProcess *process = this->buildTaskProcess();
    process->setArguments(QStringList() << "export");
    process->start();
    process->waitForFinished();
    auto newTasks = this->readTasksFromOutput(process);
    qDeleteAll(tasks.begin(), tasks.end());
    tasks.clear();
    m_projects.clear();
    m_projects.insert(""); // No project
    m_tags.clear();
    m_tags.insert(""); // No tag
    foreach (Task *task, newTasks) {
        this->AddTask(task);
        if (task->Status() == Task::StatusPending)
        {
            pendingTasksCount++;
        }
    }
    process->deleteLater();

    if (m_pendingTasksCount != pendingTasksCount)
    {
        m_pendingTasksCount = pendingTasksCount;
        emit pendingTasksCountChanged();
    }

    return;
}

bool TasksItemModel::AddTask(Task *task, Task *taskToReplace)
{
    m_projects.insert(task->Project());
    foreach(QString tag, task->Tags())
    {
        m_tags.insert(tag);
    }

    if (task->Status() & _statusFilter)
    {
        if (taskToReplace == nullptr)
        {
            tasks.append(task);
        } else {
            auto index = this->tasks.indexOf(taskToReplace);
            this->tasks.replace(index, task);
            emit dataChanged(this->createIndex(index, 0), this->createIndex(index, TaskModelIndex::_LAST_INDEX));
        }
        return true;
    } else {
        if (taskToReplace != nullptr)
        {
            auto index = this->tasks.indexOf(taskToReplace);
            this->removeRow(index);
        }
        task->deleteLater();
        return false;
    }
}

QList<Task*> TasksItemModel::readTasksFromOutput(QProcess *process)
{
    QByteArray output = process->readAllStandardOutput();
    QJsonDocument jsonDoc = QJsonDocument::fromJson(output);
    QJsonArray tasksArray = jsonDoc.array();
    QList<Task*> result;
    foreach (QJsonValue t, tasksArray) {
        result.append(new Task(t.toObject(), this));
    }

    return result;
}

void TasksItemModel::ReloadTasks()
{
    emit layoutAboutToBeChanged();
    LoadTasks();
    emit layoutChanged();
}

void TasksItemModel::SaveTask(Task *task)
{
    QJsonDocument jsonDoc = QJsonDocument(task->toJSON());
    QByteArray input = jsonDoc.toJson();
    QProcess *process = this->buildTaskProcess();
    process->setArguments(QStringList() << "import");
    process->start();
    process->write(input);
    process->closeWriteChannel();
    process->waitForFinished();
    if (task->UUID().isEmpty()) {
        QByteArray output= process->readAll();
        QString outstr = QString::fromStdString(output.toStdString());
        QStringList lines = outstr.split("\n");
        foreach (QString line, lines) {
            if (line.trimmed().startsWith("add"))
            {
                QStringList words = line.split(" ", QString::SplitBehavior::SkipEmptyParts);
                QString new_uuid = words.at(1);
                task->setUUID(new_uuid);
            }
        }
    }
    process->deleteLater();

    auto exportProc = this->runCommandOnTask(task, QStringList("export"));
    auto newTask = this->readTasksFromOutput(exportProc).first();
    this->AddTask(newTask, task);
    exportProc->deleteLater();
    // TODO: check for errors
    return;
}

QProcess* TasksItemModel::runCommandOnTask(Task *task, QStringList arguments)
{
    QProcess *process = this->buildTaskProcess();
    QStringList allArguments = QStringList() << task->UUID() << arguments;
    process->setArguments(allArguments);
    process->start();
    process->waitForFinished();
    return process;
}

void TasksItemModel::StartTask(Task *task)
{
    QProcess* process = this->runCommandOnTask(task, QStringList("start"));
    process->deleteLater();
}

void TasksItemModel::StopTask(Task *task)
{
    QProcess* process = this->runCommandOnTask(task, QStringList("stop"));
    process->deleteLater();
}

int TasksItemModel::rowCount(const QModelIndex &) const
{
    return tasks.count();
}

int TasksItemModel::columnCount(const QModelIndex &) const
{
    return TaskModelIndex::_LAST_INDEX + 1;
}

QHash<int, QByteArray> TasksItemModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[Qt::UserRole + UrgencyIndex] = "urgency";
    roles[Qt::UserRole + DescriptionIndex] = "description";
    roles[Qt::UserRole + DueDateIndex] = "dueDate";
    roles[Qt::UserRole + UUIDIndex] = "uuid";
    roles[Qt::UserRole + StatusIndex] = "status";
    roles[Qt::UserRole + PriorityIndex] = "priority";
    roles[Qt::UserRole + WaitDateIndex] = "waitDate";
    roles[Qt::UserRole + ProjectIndex] = "project";
    roles[Qt::UserRole + TagsIndex] = "tags";
    roles[Qt::UserRole + AnnotationsIndex] = "annotations";
    roles[Qt::UserRole + EntryDateIndex] = "entryDate";
    return roles;
}

QVariant TasksItemModel::data(const QModelIndex &index, int role) const
{
    if (role >= Qt::UserRole)
    {
        QModelIndex siblingIndex;
        // siblingAtColumn is available starting with QT 5.11
#if (QT_VERSION >= QT_VERSION_CHECK(5, 11, 0))
        siblingIndex = index.siblingAtColumn(role - Qt::UserRole);
#else
        siblingIndex = index.sibling(index.row(), role - Qt::UserRole);
#endif
        return this->data(siblingIndex, Qt::EditRole);
    }
    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        Task *task= taskAtIndex(index);

        switch (TaskModelIndex(index.column()))
        {
        case IdIndex:
            return task->Id();
        case DescriptionIndex:
            return task->Description();
        case DueDateIndex:
        {
            QDateTime dueDate = task->DueDate();
            if (role == Qt::DisplayRole) {
                return this->formatRelativeDateTime(dueDate, QLocale::ShortFormat);
            } else {
                return dueDate;
            }
        }
        case UrgencyIndex:
            return task->Urgency();
        case UUIDIndex:
            return task->UUID();
        case StatusIndex:
        {
            Task::TaskStatus status = task->Status();
            if (role == Qt::DisplayRole)
            {
                switch (status)
                {
                case Task::StatusCompleted:
                    return tr("Completed");
                case Task::StatusPending:
                    return tr("Pending");
                case Task::StatusDeleted:
                    return tr("Deleted");
                case Task::StatusWaiting:
                    return tr("Waiting");
                default:
                    return tr("Unknown status");
                }
            }
            else
            {
                return status;
            }
        }
        case PriorityIndex:
        {
            Task::TaskPriority priority = task->Priority();
            if (role == Qt::DisplayRole)
            {
                switch (priority) {
                case Task::NoPriority:
                    return tr("No priority");
                case Task::LowPriority:
                    return tr("Low");
                case Task::MediumPriority:
                    return tr("Medium");
                case Task::HightPriority:
                    return tr("High");
                }
            }
            else
            {
                return priority;
            }
        }
        case WaitDateIndex:
        {
            QDateTime waitDate = task->WaitDate();
            if (role == Qt::DisplayRole) {
                return this->formatRelativeDateTime(waitDate, QLocale::ShortFormat);
            } else {
                return waitDate;
            }
        }
        case ProjectIndex:
            return task->Project();
        case TagsIndex:
        {
            QStringList tags = task->Tags();
            if (role == Qt::DisplayRole)
            {
                return tags.join(", ");
            }
            else
            {
                return task->Tags();
            }
        }
        case AnnotationsIndex:
        {
            QVariantList res;
            foreach(TaskAnnotation annon, task->Annotations())
            {
                res << QVariant::fromValue(annon);
            }
            return res;
        }
        case EntryDateIndex:
        {
            QDateTime entryDate = task->EntryDate();
            if (role == Qt::DisplayRole) {
                return this->formatRelativeDateTime(entryDate, QLocale::ShortFormat);
            } else {
                return entryDate;
            }
        }
        }
    }
    return QVariant();
}

QVariant TasksItemModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal)
        {
            switch (TaskModelIndex(section))
            {
            case IdIndex:
                return tr("Id");
            case DescriptionIndex:
                return tr("Description");
            case DueDateIndex:
                return tr("Due date");
            case UrgencyIndex:
                return tr("Urgency");
            case UUIDIndex:
                return tr("UUID");
            case StatusIndex:
                return tr("Status");
            case PriorityIndex:
                return tr("Priority");
            case WaitDateIndex:
                return tr("Wait until");
            case ProjectIndex:
                return tr("Project");
            case TagsIndex:
                return tr("Tags");
            case AnnotationsIndex:
                return tr("Annotations");
            case EntryDateIndex:
                return tr("Entry date");
            }
        }
    }
    return QVariant();
}

Qt::ItemFlags TasksItemModel::flags(const QModelIndex &) const
{
    return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

bool TasksItemModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role == Qt::EditRole) {
        Task *task= taskAtIndex(index);
        switch (TaskModelIndex(index.column())) {
        case DescriptionIndex:
            task->setDescription(value.toString());
            break;
        case DueDateIndex:
            task->setDueDate(value.toDateTime());
            break;
        case PriorityIndex:
            task->setPriority(Task::TaskPriority(value.toInt()));
            break;
        case WaitDateIndex:
            task->setWaitDate(value.toDateTime());
            break;
        case ProjectIndex:
            task->setProject(value.toString());
            break;
        case TagsIndex:
            task->setTags(value.toStringList());
            break;
        default:
            return false;
        }
        task->_hasUnsavedChanges = true;
        emit dataChanged(index, index);
        return true;
    }
    return false;
}

bool TasksItemModel::removeRows(int row, int count, const QModelIndex &)
{
    if (count != 1)
    {
        return false;
    }
    else {
        emit beginRemoveRows(QModelIndex(), row, row);
        tasks.removeAt(row);
        emit endRemoveRows();
        return true;
    }
}

void TasksItemModel::sort(int column, Qt::SortOrder order)
{
    auto comparer = [column, order] (Task *a, Task *b) -> bool {
        bool result = false;
        switch (TaskModelIndex(column)) {
        case UrgencyIndex:
            result = a->Urgency() < b->Urgency();
            break;
        case DescriptionIndex:
            result = a->Description() < b->Description();
            break;
        case DueDateIndex:
            result = a->DueDate() < b->DueDate();
            break;
        case StatusIndex:
            result = a->Status() < b->Status();
            break;
        case PriorityIndex:
            result = a->Priority() < b->Priority();
            break;
        case WaitDateIndex:
            result = a->WaitDate() < b->WaitDate();
            break;
        case ProjectIndex:
            result = a->Project() < b->Project();
            break;
        case EntryDateIndex:
            result = a->EntryDate() < b->EntryDate();
            break;
        default:
            break;
        }
        if (order == Qt::DescendingOrder)
        {
            result = !result;
        }
        return result;
    };

    emit layoutAboutToBeChanged();
    std::stable_sort(tasks.begin(), tasks.end(), comparer);
    emit layoutChanged();
}


Task* TasksItemModel::taskAtIndex(const QModelIndex &index) const
{
    return tasks.at(index.row());
}

Task* TasksItemModel::taskAtRow(const int &row)
{
    return tasks.at(row);
}

Task::TaskStatuses TasksItemModel::StatusFilter()
{
    return _statusFilter;
}

void TasksItemModel::setStatusFilter(Task::TaskStatuses filter)
{
    _statusFilter = filter;
    ReloadTasks();
}

QStringList TasksItemModel::Projects()
{
    QStringList list = m_projects.values();
    list.sort();
    return list;
}

QStringList TasksItemModel::Tags()
{
    QStringList list = m_tags.values();
    list.sort();
    return list;
}

QModelIndex TasksItemModel::newTask()
{
    int newRow = tasks.count();
    emit beginInsertRows(QModelIndex(), newRow, newRow);
    QJsonObject object;
    Task *task = new Task(object, this);
    tasks.append(task);
    emit endInsertRows();
    return this->createIndex(newRow, 1);
}

QString TasksItemModel::formatRelativeDateTime(QDateTime date, QLocale::FormatType format) const
{
    if (date.isNull() || !date.isValid()) {
        return "";
    } else {
        return KFormat().formatRelativeDateTime(date, format);
    }
}

bool TasksItemModel::submit()
{
    foreach(Task *task, this->tasks)
    {
        if (task->_hasUnsavedChanges)
        {
            this->SaveTask(task);
        }
    }
    return true;
}
