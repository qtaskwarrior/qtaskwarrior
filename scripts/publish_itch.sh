#!/bin/sh

# SPDX-FileCopyrightText: 2021 Jaime Marquínez Ferrándiz <jaime.marquinez.ferrandiz@fastmail.net>
#
# SPDX-License-Identifier: Unlicense

# Exit on error
set -e

ITCH_APPNAME=jaimeMF/qtaskwarrior
CHANNEL_APPIMAGE=linux-appimage
CHANNEL_APPDIR=linux-appdir
CHANNEL_SOURCE_CODE=source-code

RELEASE_DIR=release

if test -d "$RELEASE_DIR"; then
    rm -r "$RELEASE_DIR"
fi
echo Getting binaries from Open Build Service
osc getbinaries home:jaimeMF:qtaskwarrior qtaskwarrior AppImage x86_64 --destdir="$RELEASE_DIR"
pushd "$RELEASE_DIR"
APPIMAGE_FILE=`echo ./qtaskwarrior-*.AppImage`
VERSION=`echo $APPIMAGE_FILE | sed -e 's@./qtaskwarrior-@@' -e 's/-Build.*//'`
REVISION=`echo $VERSION | sed -e 's/.*\.//'`
echo AppImage version is $VERSION
echo HG revision is $REVISION

publish_appimage()
{
    echo Publishing AppImage
    butler push $APPIMAGE_FILE $ITCH_APPNAME:$CHANNEL_APPIMAGE --userversion=$VERSION
}

publish_appdir()
{
    echo Extracting AppDir
    "$APPIMAGE_FILE" --appimage-extract
    APP_DIR=squashfs-root
    pushd $APP_DIR
    rm -r etc
    rm -r var
    mv AppRun qtaskwarrior
    popd

    echo Publishing AppDir
    butler push $APP_DIR $ITCH_APPNAME:$CHANNEL_APPDIR --userversion=$VERSION
}

publish_source()
{
    echo Exporting source
    SOURCE_DIR=source
    hg archive --rev $REVISION --type files $SOURCE_DIR
    echo Publishing source code
    butler push $SOURCE_DIR $ITCH_APPNAME:$CHANNEL_SOURCE_CODE --userversion=$VERSION
}

publish_source
publish_appimage
publish_appdir
