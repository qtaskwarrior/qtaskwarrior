.PHONY: lint lint-reuse lint-appstream

lint: lint-reuse lint-appstream

lint-reuse:
	reuse lint

lint-appstream:
	appstreamcli validate io.sourceforge.qtaskwarrior.appdata.xml
